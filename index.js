const AjaxHelper = require(`./Modules/AjaxHelper/AjaxHelper`);
const dtv = require('dotenv').config({ path: `.env`});
if (dtv.error) {
    throw dtv.error
}
const ExcelGenerator = require(`./Modules/ExcelGenerator/ExcelGenerator`);

async function init() {
    const data = await AjaxHelper.getData();
    ExcelGenerator.getFile(data.products);
}
init();

