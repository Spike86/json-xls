const axios = require(`axios`);

class AjaxHelper {
    async getData() {
        console.log(process.env.PRODUCT_URL);
        return await axios.get(process.env.PRODUCT_URL).then(res => {
            return res.data;
        }).catch(res => {
            console.log(res)
            return res;
        })
    }
}

module.exports = new AjaxHelper();


