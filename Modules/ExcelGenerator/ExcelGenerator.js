const xl = require('excel4node');
const headerStyle = {
    alignment: {
        vertical: [`center`]
    },
    font: {
        color: '#ffffff'
    },
    fill: {
        type: 'pattern',
        patternType: 'solid',
        fgColor: '2172d7'
    }
};

class ExcelGenerator {
    constructor() {
        this.wb = new xl.Workbook();
        this.ws = this.wb.addWorksheet('Sheet 1');
    }

    getFile(data) {
        this.prepareHeader();
        this.fillWorkSheet(data);
        const now = new Date();
        const fileName = `${now.getDate()}_${now.getMonth() + 1}__${now.getHours()}_${now.getMinutes()}_${now.getSeconds()}`;
        this.wb.write(`./OutputFiles/${fileName}.xlsx`);
    }

    fillWorkSheet(data) {
        const ws = this.ws;
        data.forEach(function (el, index) {
            const row = index + 2;
            ws.cell(row, 1).string(el.asin);
            ws.cell(row, 2).string(el.categoryTree.map(item => `${item.name}, `));
            ws.cell(row, 3).string(el.upcList || `Нет значения`);
            ws.cell(row, 4).string(el.eanList || `Нет значения`);
            ws.cell(row, 5).string(el.brand);
            ws.cell(row, 6).number(el.numberOfItems);
            ws.cell(row, 7).number(el.availabilityAmazon);
            ws.cell(row, 8).string(el.availabilityAmazonDelay || `Нет значения`);
            ws.cell(row, 9).bool(el.isAdultProduct);
            ws.cell(row, 10).number(el.csv[3][el.csv[3].length - 1] || `Нет значения`);
            ws.cell(row, 11).string(el.ebayListingIds || `Нет значения`);
            ws.cell(row, 12).number(el.csv[1][el.csv[1].length - 1] / 100 || `Нет значения`);
        })
    }

    prepareHeader() {
        this.ws.cell(1, 1).string(`Asin`).style(headerStyle);
        this.ws.cell(1, 2).string(`categoryTree`).style(headerStyle);
        this.ws.cell(1, 3).string(`upcList`).style(headerStyle);
        this.ws.cell(1, 4).string(`eanList`).style(headerStyle);
        this.ws.cell(1, 5).string(`brand`).style(headerStyle);
        this.ws.cell(1, 6).string(`numberOfItems`).style(headerStyle);
        this.ws.cell(1, 7).string(`availabilityAmazon`).style(headerStyle);
        this.ws.cell(1, 8).string(`availabilityAmazonDelay`).style(headerStyle);
        this.ws.cell(1, 9).string(`isAdultProduct`).style(headerStyle);
        this.ws.cell(1, 10).string(`salesRanks`).style(headerStyle);
        this.ws.cell(1, 11).string(`ebayListingIds`).style(headerStyle);
        this.ws.cell(1, 12).string(`Price`).style(headerStyle);
    }
}

module.exports = new ExcelGenerator();